zxvcv/conan
===========
Image created mainly to build conan packages with diffrent compilers.

Run docker image in interactive mode
------------------------------------
```
docker run -ti zxvcv/conan:latest
```

Run docker images in interactive mode as current user:
```
docker run -ti -u $(id -u ${USER}):$(id -g ${USER}) zxvcv/conan:latest
```

Pushing docker image to dockerhub
---------------------------------
Dockerhub authentication: `docker login`
Push image to dockerhub: `docker push zxvcv/conan:latest`

Helps
-----
```
service docker start
docker run -ti -w=/home/conan/pckg -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg zxvcv/conan:latest
docker build -t zxvcv/conan:latest
```

References:
-----------
- Repository: https://gitlab.com/zxvcv-docker/conan
- Docker Hub: https://hub.docker.com/repository/docker/zxvcv/conan
