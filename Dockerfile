# syntax=docker/dockerfile:1
FROM ubuntu:latest
WORKDIR /app

RUN apt-get update
RUN apt-get install -y curl bzip2

# install conan
RUN apt-get install -y python3 python3-pip
RUN python3 -m pip install -U conan==1.51.2

# install cmake
RUN apt-get install -y cmake

ENV PATH="/usr/bin:/usr/local/bin:$PATH"
