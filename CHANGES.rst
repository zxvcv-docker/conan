Changelog
=========

1.0.1 (2022-09-24)
------------------
- Fix for 1.0.0, add paths to PATH
- README updates

1.0.0 (2022-09-07)
------------------
- Initial package for zxvcv/conan v1.51.2
